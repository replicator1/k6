import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      // EXAMPLE 1
      Graph graph = new Graph("Example graph 1:");

      Vertex v5 = graph.createVertex("v5");
      Vertex v4 = graph.createVertex("v4");
      Vertex v3 = graph.createVertex("v3");
      Vertex v2 = graph.createVertex("v2");
      Vertex v1 = graph.createVertex("v1");

      graph.createArcs(v1, v2);
      graph.createArcs(v1, v3);
      graph.createArcs(v1, v4);
      graph.createArcs(v1, v5);


      System.out.println(graph);
      System.out.println("Method findIndependentSet with input 3 returns: "
              + graph.findIndependentSet(3) + ", expected {v2, v3, v4} or {v2, v3 ,v5} or {v3, v4, v5}");
      System.out.println("Method findIndependentSet with input 4 returns: "
              + graph.findIndependentSet(4) + ", expected {v2, v3, v4, v5}");
      System.out.println("Method findIndependentSet with input 5 returns: "
              + graph.findIndependentSet(5) + ", expected null");

      // EXAMPLE 2
      Graph graph2 = new Graph("Example graph 2:");

      Vertex vv8 = graph2.createVertex("v8");
      Vertex vv7 = graph2.createVertex("v7");
      Vertex vv6 = graph2.createVertex("v6");
      Vertex vv5 = graph2.createVertex("v5");
      Vertex vv4 = graph2.createVertex("v4");
      Vertex vv3 = graph2.createVertex("v3");
      Vertex vv2 = graph2.createVertex("v2");
      Vertex vv1 = graph2.createVertex("v1");

      graph2.createArcs(vv1, vv2);
      graph2.createArcs(vv1, vv4);
      graph2.createArcs(vv2, vv5);
      graph2.createArcs(vv3, vv5);
      graph2.createArcs(vv3, vv7);
      graph2.createArcs(vv4, vv5);
      graph2.createArcs(vv5, vv8);
      graph2.createArcs(vv3, vv6);

      System.out.println(graph2);
      System.out.println("Method findIndependentSet with input 4 returns: "
              + graph2.findIndependentSet(4) + ", expected {v2, v3, v4, v8} or {v1, v5, v6, v7}"
              + " or {v2, v4, v8, v6} or {v2, v4, v8, v7}"
              + "or {v8, v7, v6, v4} or { v8, v7, v6, v2} or { v8, v7, v6, v1}");
      System.out.println("Method findIndependentSet with input 5 returns: "
              + graph2.findIndependentSet(5) + ", expected {v8, v7, v6, v4, v2}");
      System.out.println("Method findIndependentSet with input 6 returns: "
              + graph2.findIndependentSet(6) + ", expected null");

      // EXAMPLE 3
      Graph graph3 = new Graph("Example graph 3:");

      Vertex vvv5 = graph3.createVertex("v5");
      Vertex vvv4 = graph3.createVertex("v4");
      Vertex vvv3 = graph3.createVertex("v3");
      Vertex vvv2 = graph3.createVertex("v2");
      Vertex vvv1 = graph3.createVertex("v1");

      graph3.createArcs(vvv1, vvv2);
      graph3.createArcs(vvv1, vvv3);
      graph3.createArcs(vvv1, vvv5);
      graph3.createArcs(vvv3, vvv2);
      graph3.createArcs(vvv4, vvv5);

      Graph graph3C = new Graph("Example graph 3 hand-made compliment:");

      Vertex vvv5C = graph3C.createVertex("v5");
      Vertex vvv4C = graph3C.createVertex("v4");
      Vertex vvv3C = graph3C.createVertex("v3");
      Vertex vvv2C = graph3C.createVertex("v2");
      Vertex vvv1C = graph3C.createVertex("v1");

      graph3C.createArcs(vvv1C, vvv4C);
      graph3C.createArcs(vvv2C, vvv5C);
      graph3C.createArcs(vvv2C, vvv4C);
      graph3C.createArcs(vvv5C, vvv3C);
      graph3C.createArcs(vvv4C, vvv3C);

      System.out.println(graph3);
      System.out.println("Graph compliment made by crateCompliment method");
      System.out.println(graph3.createCompliment());
      System.out.println(graph3C);

      // EXAMPLE 4
      Graph graph4 = new Graph("Example graph 4 with 300 vertices and 300 arcs");
      graph4.createRandomSimpleGraph(300, 300);
      long startTime = System.currentTimeMillis();
      graph4.findIndependentSet(100);
      long endTime =  System.currentTimeMillis();
      System.out.println("Finding independent set of size 100 in graph with" +
              " 300 vertices and 300 arcs took: " + (endTime - startTime) + " milliseconds or " +
              ((endTime - startTime) / 1000.0) + " seconds");

      Graph graph5 = new Graph("Example graph 5 with 1000 vertices and 1000 arcs");
      graph5.createRandomSimpleGraph(1000, 1000);
      startTime = System.currentTimeMillis();
      graph5.findIndependentSet(400);
      endTime =  System.currentTimeMillis();
      System.out.println("Finding independent set of size 400 in graph with" +
              " 1000 vertices and 1000 arcs took: " + (endTime - startTime) + " milliseconds or " +
              ((endTime - startTime) / 1000.0) + " seconds");

      Graph graph6 = new Graph("Example graph 6 with 2000 vertices and 2000 arcs");
      graph6.createRandomSimpleGraph(2000, 2000);
      startTime = System.currentTimeMillis();
      graph6.findIndependentSet(800);
      endTime =  System.currentTimeMillis();
      System.out.println("Finding independent set of size 800 in graph with" +
              " 2000 vertices and 2000 arcs took: " + (endTime - startTime) + " milliseconds or " +
              ((endTime - startTime) / 1000.0) + " seconds");

      //EXAMPLE 5
      Graph graph7 = new Graph("Example cube graph");

      Vertex ve8 = graph7.createVertex("v8");
      Vertex ve7 = graph7.createVertex("v7");
      Vertex ve6 = graph7.createVertex("v6");
      Vertex ve5 = graph7.createVertex("v5");
      Vertex ve4 = graph7.createVertex("v4");
      Vertex ve3 = graph7.createVertex("v3");
      Vertex ve2 = graph7.createVertex("v2");
      Vertex ve1 = graph7.createVertex("v1");

      graph7.createArcs(ve1, ve2);
      graph7.createArcs(ve1, ve3);
      graph7.createArcs(ve1, ve5);
      graph7.createArcs(ve2, ve4);
      graph7.createArcs(ve2, ve6);
      graph7.createArcs(ve4, ve3);
      graph7.createArcs(ve4, ve8);
      graph7.createArcs(ve3, ve7);
      graph7.createArcs(ve5, ve6);
      graph7.createArcs(ve8, ve6);
      graph7.createArcs(ve7, ve8);
      graph7.createArcs(ve5, ve7);

      System.out.println(graph7);
      System.out.println("Method findIndependentSet with input 4 returns: "
              + graph7.findIndependentSet(4) + ", expected {v1, v6, v7, v4} or {v3, v8, v5, v2}");
      System.out.println("Method findIndependentSet with input 5 returns: "
              + graph7.findIndependentSet(5) + ", expected null");
      System.out.println("Method findIndependentSet with input 6 returns: "
              + graph7.findIndependentSet(6) + ", expected null");

      // EXAMPLE 6
      System.out.println();
      Graph graph8 = new Graph("Example 6");
      graph8.createRandomSimpleGraph(100, 100);
      List<Vertex> indep = graph8.findIndependentSet(40);

      if (indep != null) {
         System.out.println("Found independent set of size 40 in graph with 100 vertices and 100 arcs");
         System.out.println(indep);
         System.out.println("Found independent set size: " + indep.size() + ", expected 40");

         System.out.println("Control if found set is independent");
         System.out.println("Output: ");
         boolean co = false;
         for (Vertex vertex: indep) {
            for (Vertex vertex2: indep) {
               if (vertex != vertex2 && !vertex.isConnectedTo(vertex2)) {
                  co = true;
                  System.out.println("Set is not independent! Vertices " + vertex + " " + vertex2 + " are connected!");
               }
            }
         }
         if (!co) System.out.println("Example graph 6 found set of size 40 is independent");
      }
   }

   /**
    * Problem: Find if given Graph contains independent set of given size.
    * Explanation: Every set is considered independent if it forms
    * clique in compliment graph. To find a clique in compliment graph I used
    * the Bron-Kerbosch algorithm.
    * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
    * https://iq.opengenus.org/bron-kerbosch-algorithm/
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /**
       * Calculate number of connected vertices (Arcs)
       *
       * @return number of arcs
       */
      public int getNumberOfConnections() {
         int result = 0;

         Arc base = first;
         while (base != null) {
            result++;
            base = base.next;
         }
         return result;
      }

      /**
       * Check in is connection (Arc) with Vertex
       *
       * @param other Vertex to check connection with
       * @return true if it is connection
       */
      public boolean isConnectedTo(Vertex other) {
         if (Objects.equals(id, other.id)) return false;

         Arc base = first;
         while (base != null) {
            if (Objects.equals(base.target.id, other.id)) return true;
            base = base.next;
         }
         return false;
      }
   }

   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Create simple 2 way connection between Vertices.
       * Used only in tests.
       */
      public void createArcs(Vertex a, Vertex b) {
         String nameAB = String.format("a%s_%s", a.toString(), b.toString());
         String nameBA = String.format("a%s_%s", b.toString(), a.toString());
         this.createArc(nameAB, a, b);
         this.createArc(nameBA, b, a);
      }


      /**
       * Find independent set of given size.
       * Set is independent if there is a clique in compliment graph.
       *
       * @param size size of set to search
       * @return true if set of given size is found
       */
      public List<Vertex> findIndependentSet(int size) {
         Graph complimentGraph = createCompliment();

         List<Vertex> vertices = new ArrayList<>();

         // Implementation can be optimized if deleted all vertices witch haw less connections than needed
         Vertex base = complimentGraph.first;
         while (base != null) {
            if (base.getNumberOfConnections() >= size - 1) {
               vertices.add(base);
            }
            base = base.next;
         }

         List<Vertex> tempA = new ArrayList<>();
         List<Vertex> tempB = new ArrayList<>();

         return findClique(tempA, vertices, tempB, 0, size);
      }

      /**
       * Create compliment of the graph.
       *
       * @return compliment graph
       */
      public Graph createCompliment() {
         Graph complimentGraph = new Graph(this.id);
         List<Vertex> vertices = new ArrayList<>();

         Vertex base = this.first;
         while (base != null) {
            vertices.add(complimentGraph.createVertex(base.id));
            base = base.next;
         }

         base = this.first;
         int indexBase = 0;
         while (base != null) {

            int indexOther = 0;
            Vertex other = this.first;
            while (other != null) {

               if (!Objects.equals(other.id, base.id) && !base.isConnectedTo(other))
                  complimentGraph.createArc("a" + base.id + "_" + other.id,
                          vertices.get(indexBase), vertices.get(indexOther));

               indexOther++;
               other = other.next;
            }
            indexBase++;
            base = base.next;
         }
         return complimentGraph;
      }

      /**
       * Find clique of given size.
       * Algorithm is based on Bron–Kerbosch algorithm.
       * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
       * https://iq.opengenus.org/bron-kerbosch-algorithm/
       *
       * @param potentialClique   clique algorithm is currently processing
       * @param remainingVertices all remaining vertices which algorithm is not proceed yet
       * @param skipVertices      vertices already processed by algorithm. Found maximal clique for each vertex
       * @param depth             size of current potential clique
       * @param k                 given size to found
       * @return List<Vertex> (potentialClique) if clique of given size is found, null if not found
       */
      public List<Vertex> findClique(List<Vertex> potentialClique, List<Vertex> remainingVertices,
                                List<Vertex> skipVertices, int depth, int k) {

         // Found maximal clique for this vertex
         if (remainingVertices.isEmpty() && skipVertices.isEmpty()) return null;

         int index = 0;
         while (index < remainingVertices.size()) {
            Vertex vertex = remainingVertices.get(index);
            index++;

            // Try adding each vertex to potential clique

            List<Vertex> newPotentialClique = new ArrayList<>(potentialClique);
            newPotentialClique.add(vertex);

            // Clique of given size found
            if (depth + 1 == k) return newPotentialClique;

            List<Vertex> newRemainingVertices = new ArrayList<>();
            for (Vertex remainingVertex : remainingVertices) {
               if (vertex.isConnectedTo(remainingVertex)) {
                  newRemainingVertices.add(remainingVertex);
               }
            }

            List<Vertex> newSkipVertices = new ArrayList<>();
            for (Vertex skipVertex : skipVertices) {
               if (vertex.isConnectedTo(skipVertex)) {
                  newSkipVertices.add(skipVertex);
               }
            }

            List<Vertex> result = findClique(newPotentialClique, newRemainingVertices, newSkipVertices, depth + 1, k) ;
            // clique of size k is found returns it
            if (result != null) return result;

            // Maximal clique is found. Vertex can be removed from remaining and added to skip list
            remainingVertices.remove(vertex);
            index = index - 1;
            skipVertices.add(vertex);
         }
         return null;
      }
   }
}

