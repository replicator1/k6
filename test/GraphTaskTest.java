import static org.junit.Assert.*;

import org.apache.commons.math3.util.CombinatoricsUtils;
import org.junit.Test;

import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test(timeout = 20000)
   public void simpleTest() {
      GraphTask.main(null);

      GraphTask g = new GraphTask();

      GraphTask.Graph graph = g.new Graph("G1");

      GraphTask.Vertex v5 = graph.createVertex("v5");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      graph.createArcs(v1, v2);
      graph.createArcs(v1, v3);
      graph.createArcs(v1, v4);
      graph.createArcs(v1, v5);


      graph.findIndependentSet(5);
      /*
      assertTrue("size 3 independent set should be found, formed by v1 v2 v3", graph.findIndependentSet(3));
      assertTrue("size 4 independent set should be found, formed by v1 v2 v3 v4", graph.findIndependentSet(4));
      assertFalse("size 5 independent set should NOT be found", graph.findIndependentSet(5));

       */
   }

   @Test(timeout = 20000)
   public void complimentTest() {
      GraphTask.main(null);

      GraphTask g = new GraphTask();

      GraphTask.Graph graph = g.new Graph("G1");

      GraphTask.Vertex v6 = graph.createVertex("v6");
      GraphTask.Vertex v5 = graph.createVertex("v5");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      graph.createArcs(v1, v2);
      graph.createArcs(v1, v5);
      graph.createArcs(v1, v4);
      graph.createArcs(v2, v3);
      graph.createArcs(v3, v6);
      graph.createArcs(v4, v5);

      System.out.println(graph.createCompliment());

      GraphTask.Graph graph2 = g.new Graph("G2");

      GraphTask.Vertex vv6 = graph2.createVertex("v6");
      GraphTask.Vertex vv5 = graph2.createVertex("v5");
      GraphTask.Vertex vv4 = graph2.createVertex("v4");
      GraphTask.Vertex vv3 = graph2.createVertex("v3");
      GraphTask.Vertex vv2 = graph2.createVertex("v2");
      GraphTask.Vertex vv1 = graph2.createVertex("v1");

      graph.createArcs(vv1, vv3);
      graph.createArcs(vv1, vv6);

      graph.createArcs(vv2, vv6);
      graph.createArcs(vv2, vv5);
      graph.createArcs(vv2, vv4);

      graph.createArcs(vv3, vv5);
      graph.createArcs(vv3, vv4);

      graph.createArcs(vv4, vv6);

      graph.createArcs(vv5, vv6);
   }


   @Test(timeout = 20000)
   public void simpleTestWithException() {
      GraphTask.main(null);

      GraphTask g = new GraphTask();

      GraphTask.Graph graph2 = g.new Graph("G2");
      // EXAMPLE 2
      GraphTask.Vertex vv8 = graph2.createVertex("v8");
      GraphTask.Vertex vv7 = graph2.createVertex("v7");
      GraphTask.Vertex vv6 = graph2.createVertex("v6");
      GraphTask.Vertex vv5 = graph2.createVertex("v5");
      GraphTask.Vertex vv4 = graph2.createVertex("v4");
      GraphTask.Vertex vv3 = graph2.createVertex("v3");
      GraphTask.Vertex vv2 = graph2.createVertex("v2");
      GraphTask.Vertex vv1 = graph2.createVertex("v1");

      graph2.createArcs(vv1, vv2);
      graph2.createArcs(vv1, vv4);

      graph2.createArcs(vv2, vv5);

      graph2.createArcs(vv3, vv5);
      graph2.createArcs(vv3, vv7);

      graph2.createArcs(vv4, vv5);
      graph2.createArcs(vv5, vv8);

      graph2.createArcs(vv7, vv6);

      System.out.println(graph2);
   /*
      assertTrue("size 3 independent set should be found, formed by v1 v5 v7", graph2.findIndependentSet(3));
      assertTrue("size 4 independent set should be found, formed by v2 v3 v4 v6", graph2.findIndependentSet(4));
      assertTrue("size 5 independent set clique should be found, formed by v2 v3 v4 v6 v8", graph2.findIndependentSet(5));
      assertFalse("size 6 independent set clique should NOT be found", graph2.findIndependentSet(6));
      assertFalse("size 7 independent set clique should NOT be found", graph2.findIndependentSet(7));

    */
   }

}

